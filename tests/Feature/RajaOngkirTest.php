<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;

class RajaOngkirTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

    }

    /**
     * 
     */
    public function test_post_cost_success()
    {
        $response = $this->postJson(
            '/api/cost',
            [
                'key' => 'aa8e37a4d877ecdf7af0807dafde9670',
                'origin' => 23,
                'destination' => 14,
                'weight' => 1000,
                'courier' => 'jne'
            ]
        );

        $response
            ->assertStatus(200);
    }

    public function test_post_cost_http_status_400()
    {
        $response = $this->postJson(
            '/api/cost',
            [
                'key' => 'aa8e37a4d877ecdf7af0807dafde9671',
                'origin' => 23,
                'destination' => 14,
                'weight' => 1000,
                'courier' => 'jne'
            ]
        );

        $response
            ->assertStatus(400);
    }

    public function test_post_cost_failed_2_using_assertableJson()
    {
        $response = $this->postJson(
            '/api/cost',
            [
                'key' => 'aa8e37a4d877ecdf7af0807dafde9671',
                'origin' => 23,
                'destination' => 14,
                'weight' => 1000,
                'courier' => 'jnee'
            ]
        );

        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('status', 'Failed')
                    ->where('code', 400)
                    ->where('message', 'API request failed.')
                    ->etc()
            );
    }
    
}
