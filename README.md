# POST API Raja Ongkir

## Tentang 
API create biaya ongkir datanya (asal, tujuan, harga), untuk bisa mengetahui harganya menggunakana Raja Ongkir (https://rajaongkir.com/dokumentasi/starter). Implementasi API memanfaatkan
* Error handling
* Monitoring error menggunakan Sentry
* Unit testing

## How to run the project
* Clone this project
* Enter this command `php artisan install` on your terminal
* Run the project server `php artisan serve`

## How to run the unit testing
* Make sure you have already set up this project
* Run the unit testing using this command `php artisan test`

## API Documentation

### Cek harga ongkir

* URL
`/api/cost`

* Method
`POST`

* URL Params
    * `'key': string`
    * `'origin': integer`
    * `'destination': integer`
    * `'weight': integer`
    * `'courier': string`

* Response
    * JSON Data
        ```
            'status' => $status,
            'code' => $code,
            'message' => $message,
            'errors' => $errors,
            'data' => $data,
        ```
* Contoh Request
    * PHP
    ```
    <?php

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://127.0.0.1:8000/api/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=501&destination=114&weight=1700&courier=jne",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: your-api-key"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
        
    ```
    * Postman
        ![alt text](screenshots/ss1.png "Tes")

        ```
            {
                "status": "Successful",
                "code": 200,
                "message": "API request succeed.",
                "errors": null,
                "data": {
                    "rajaongkir": {
                        "query": {
                            "key": "aa8e37a4d877ecdf7af0807dafde9670",
                            "origin": "23",
                            "destination": "14",
                            "weight": 1000,
                            "courier": "jne"
                        },
                        "status": {
                            "code": 200,
                            "description": "OK"
                        },
                        "origin_details": {
                            "city_id": "23",
                            "province_id": "9",
                            "province": "Jawa Barat",
                            "type": "Kota",
                            "city_name": "Bandung",
                            "postal_code": "40111"
                        },
                        "destination_details": {
                            "city_id": "14",
                            "province_id": "19",
                            "province": "Maluku",
                            "type": "Kota",
                            "city_name": "Ambon",
                            "postal_code": "97222"
                        },
                        "results": [
                            {
                                "code": "jne",
                                "name": "Jalur Nugraha Ekakurir (JNE)",
                                "costs": [
                                    {
                                        "service": "OKE",
                                        "description": "Ongkos Kirim Ekonomis",
                                        "cost": [
                                            {
                                                "value": 76000,
                                                "etd": "3-5",
                                                "note": ""
                                            }
                                        ]
                                    },
                                    {
                                        "service": "REG",
                                        "description": "Layanan Reguler",
                                        "cost": [
                                            {
                                                "value": 81000,
                                                "etd": "1-2",
                                                "note": ""
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        ```