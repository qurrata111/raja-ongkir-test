<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RajaOngkir extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'status'=> $this->status,
            'code' => $this->rajaongkir->status->code
        ];
    }
}
