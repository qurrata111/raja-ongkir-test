<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;

class RajaOngkir extends Controller
{
    public function index() {
        request()->validate([
            'key' => 'required',
            'id' => 'required'
        ]);

        try {
            $response = Http::get('https://api.rajaongkir.com/starter/province', [
                'key' => request('key'),
                'id' => request('id'),
            ]);

            return $this->respond(
                ($response->status() >= 200 && $response->status() <= 299) ? "Successful" : "Failed", // string status
                $response->status(), // int code 
                $response->successful() ? "API request succeed." : "API request failed.",
                null,
                $response->json()
            );

        } catch (Throwable $exception) {
            app('sentry')->captureException($exception);
            return $this->respond(
                ($response->status() >= 200 && $response->status() <= 299) ? "Successful" : "Failed", // string status
                $response->status(), // int code 
                $response->successful() ? "API request succeed." : "API request failed.",
                $exception,
                $response->json()
            );
        }
    }

    public function store() {
        request()->validate([
            'key' => 'required',
            'origin' => 'required',
            'destination' => 'required',
            'weight' => 'required',
            'courier' => 'required'
        ]);

        try {
            $response = Http::asForm()
                            ->post('https://api.rajaongkir.com/starter/cost', [
                                'key' => request('key'),
                                'origin' => request('origin'),
                                'destination' => request('destination'),
                                'weight' => request('weight'),
                                'courier' => request('courier'),
                            ]);

            return $this->respond(
                ($response->status() >= 200 && $response->status() <= 299) ? "Successful" : "Failed", // string status
                $response->status(), // int code 
                $response->successful() ? "API request succeed." : "API request failed.",
                null,
                $response->json()
            );

        } catch (Throwable $exception) {
            app('sentry')->captureException($exception);

            return $this->respond(
                ($response->status() >= 200 && $response->status() <= 299) ? "Successful" : "Failed", // string status
                $response->status(), // int code 
                $response->successful() ? "API request succeed." : "API request failed.",
                $exception,
                $response->json()
            );
        }
    }
}