<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * JSON response
     *
     * @param string $status
     * @param int $code
     * @param mixed $errors
     * @param mixed $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond(string $status, int $code, string $message, $errors=null, $data=null)
    {
        return response()->json([
            'status' => $status,
            'code' => $code,
            'message' => $message,
            'errors' => $errors,
            'data' => $data,
        ], $code);
    }

}